{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}
module ICFP.Nanobots.Simulator
  ( Coordinate(..)
  , BotId(..)
  , NanobotState(..)
  , Harmonics(..)
  , VoxelState(..)
  , VoxelMatrix
  , emptyMatrix
  , voxelStateAt
  , SimulatorState(..)
  )
where

import qualified Data.Vector as V
import Data.Vector(Vector)
import Control.Lens hiding (Empty)

data Coordinate = Coordinate !Int !Int !Int deriving (Eq, Show)
newtype BotId = BotId Int deriving (Show, Eq, Ord)
data NanobotState = NanobotState
  { botId :: BotId
  , botPosition :: Coordinate
  , botSeeds :: Vector BotId
  } deriving Show

data Harmonics = Low | High deriving (Eq, Show)

flipHarmonics :: Harmonics -> Harmonics
flipHarmonics Low = High
flipHarmonics High = Low

data VoxelValue = Full | Empty deriving (Eq, Show)
data Groundedness = Grounded | NotGrounded deriving (Eq, Show)
data VoxelState = VoxelState VoxelValue Groundedness deriving (Eq, Show)

voxelIsEmpty :: VoxelState -> Bool
voxelIsEmpty (VoxelState v _) = v == Empty

data VoxelMatrix = VoxelMatrix
  { matrixDimension :: Int
  , matrixData :: Vector VoxelState
  } deriving Show

voxelIndex :: Coordinate -> VoxelMatrix -> Int
voxelIndex (Coordinate x y z) mat = x*r^2 + y*r + z where
  r = matrixDimension mat

emptyMatrix :: Int -> VoxelMatrix
emptyMatrix r = VoxelMatrix r $ V.replicate (r^3) (VoxelState Empty NotGrounded)

isInMatrix :: VoxelMatrix -> Coordinate -> Bool
isInMatrix m@VoxelMatrix{..} c = not (invalidIndex (voxelIndex c m) matrixData)

{-# INLINE voxelStateAt #-}
voxelStateAt :: Coordinate -> Lens' VoxelMatrix VoxelState
voxelStateAt coord f mat = fmap set $ f get where
  get = v V.! ix
  set voxel = mat {matrixData = V.update v (V.singleton (ix, voxel)) }
  v = matrixData mat
  ix = voxelIndex coord mat

data SimulatorState = SimulatorState
  { expendedEnergy :: Int
  , harmonics :: Harmonics
  , matrix :: VoxelMatrix
  , bots :: Vector NanobotState
  }

initialState :: Int -> SimulatorState
initialState r = SimulatorState
  { expendedEnergy = 0
  , harmonics = Low
  , matrix = emptyMatrix r
  , bots = V.singleton $ NanobotState
    { botId = BotId 1
    , botPosition = Coordinate 0 0 0
    , botSeeds = V.fromList $ map BotId [2..20]
    }
  }

data CoordinateDifference = CoordinateDifference !Int !Int !Int

coordinateDifference :: Coordinate -> Coordinate -> CoordinateDifference
coordinateDifference (Coordinate x1 y1 z1) (Coordinate x2 y2 z2)
  = CoordinateDifference (x2 - x1) (y2 - y1) (z2 - z1)

addDifference :: Coordinate -> CoordinateDifference -> Coordinate
addDifference (Coordinate x y z) (CoordinateDifference dx dy dz)
  = Coordinate (x + dx) (y + dy) (z + dz)

manhattanLength :: CoordinateDifference -> Int
manhattanLength (CoordinateDifference x y z) = abs x + abs y + abs z

chessboardLength :: CoordinateDifference -> Int
chessboardLength (CoordinateDifference x y z) = maximum [abs x, abs y, abs z]

newtype SLD = SLD { getSLD :: CoordinateDifference }

toSLD :: CoordinateDifference -> Maybe SLD
toSLD c@(CoordinateDifference x y z)
  | isValid = Just $ SLD c
  | otherwise = Nothing where
  isValid = active x + active y + active z == 1
            && abs (active x * x + active y * y + active z * z) <= 5
  active = abs . signum

newtype LLD = LLD { getLLD :: CoordinateDifference }

toLLD :: CoordinateDifference -> Maybe LLD
toLLD c@(CoordinateDifference x y z)
  | isValid = Just $ LLD c
  | otherwise = Nothing where
  isValid = active x + active y + active z == 1
            && abs (active x * x + active y * y + active z * z) <= 15
  active = abs . signum

newtype ND = ND { getND :: CoordinateDifference }

toND :: CoordinateDifference -> Maybe ND
toND c@(CoordinateDifference x y z)
  | isValid = Just $ ND c
  | otherwise = Nothing where
  isValid = 1 <= activeAxes && activeAxes <= 2
  activeAxes = active x + active y + active z
  active axis | abs axis == 1 = 1
              | axis == 0 = 0
              | abs axis > 1 = 10 -- axis too large

data BotCommand
  = Halt
  | Wait
  | Flip
  | SMove LLD
  | LMove SLD SLD
  | Fission ND !Int
  | Fill ND
  | FusionP ND
  | FusionS ND

invalidIndex :: Int -> Vector a -> Bool
invalidIndex n v = n < 0 || n >= V.length v

-- n = Bot index
executeCommand :: Int -> BotCommand -> SimulatorState -> Maybe SimulatorState
executeCommand n cmd s
  | invalidIndex n (bots s) = Nothing
  | otherwise = case cmd of
      Halt | isFinalState s -> Just s
      Wait -> Just s
      Flip -> Just s { harmonics = flipHarmonics $ harmonics s }
      SMove lld -> doStraightMove n lld s
      LMove sld1 sld2 -> doCurveMove n sld1 sld2 s
      Fission nd m -> doFission n nd m s
      Fill nd -> doFill n nd s
      FusionP nd -> doFusionPrimary n nd s
      FusionS nd -> doFusionSecondary n nd s

isFinalState :: SimulatorState -> Bool
isFinalState SimulatorState{..}
  = V.length bots == 1
  && botPosition (V.head bots) == Coordinate 0 0 0
  && harmonics == Low

doStraightMove :: Int -> LLD -> SimulatorState -> Maybe SimulatorState
doStraightMove n (LLD d) s@SimulatorState{..}
  | isValidMove = Just s
    { expendedEnergy = newEnergy
    , bots = botsAfterMove
    }
  | otherwise = Nothing where
  isValidMove = isValidEndpoint
                && V.all (\pos -> voxelIsEmpty $ matrix ^. voxelStateAt pos) betweenPositions
                && V.all (\NanobotState{..} -> not $ V.elem botPosition betweenPositions) bots
  isValidEndpoint = isInMatrix matrix $ addDifference currentBotPos d
  betweenPositions = case d of
    CoordinateDifference dx 0 0 | abs dx > 0 -> V.fromList [Coordinate (bx + dx') by bz | dx' <- map (* signum dx) [1.. abs dx]]
    CoordinateDifference 0 dy 0 | abs dy > 0 -> V.fromList [Coordinate bx (by + dy') bz | dy' <- map (* signum dy) [1.. abs dy]]
    CoordinateDifference 0 0 dz | abs dz > 0 -> V.fromList [Coordinate bx by (bz + dz') | dz' <- map (* signum dz) [1.. abs dz]]
    _                                        -> error "Invariant violated, difference wasn't LLD"
  currentBotPos@(Coordinate bx by bz) = botPosition currentBot
  currentBot = bots V.! n
  botsAfterMove = V.update bots $ V.singleton (n, currentBot {botPosition = addDifference currentBotPos d})
  newEnergy = expendedEnergy + 2 * manhattanLength d


doCurveMove :: Int -> SLD -> SLD -> SimulatorState -> Maybe SimulatorState
doCurveMove = _

doFission :: Int -> ND -> Int -> SimulatorState -> Maybe SimulatorState
doFission = _

doFill :: Int -> ND -> SimulatorState -> Maybe SimulatorState
doFill = _

doFusionPrimary :: Int -> ND -> SimulatorState -> Maybe SimulatorState
doFusionPrimary = _

doFusionSecondary :: Int -> ND -> SimulatorState -> Maybe SimulatorState
doFusionSecondary = _
